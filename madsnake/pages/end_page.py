# MadSnake - A snake game
# Copyright (C) 2020-2021  zhiqiang.bzq
#
# See LICENSE. And if you have interest in this project, join us.
#
# zhiqiang.bzq
# bzq23@foxmail.com

import pygame
from color import Color
from common import *
from pagectr import *

class EndPage(object):
  def __init__(self):
    self.isover_ = False
    self.result_ = None
    self.result_ = None

  def OnLoad(self, result):
    self.isover_ = False
    self.result_ = result

  def OnKeyboard(self, playerid, action):
    if playerid is not None: return
    if action == pygame.K_SPACE:
      self.isover_ = True
    
  def OnRender(self, screen):
    if self.result_ is None or self.result_.result is None: return
    title_font = pygame.font.SysFont('arial', 48, True)
    title_words = title_font.render(self.result_.result.message, True, Color.RandomColor(), Color.BLACK)
    subtitle_font = pygame.font.SysFont('arial', 16, True)
    subtitle_words = subtitle_font.render("Press SPACE to continue!", True, Color.WHITE, Color.BLACK)

    y = (screen.get_height() - (title_words.get_height() + subtitle_words.get_height() + 10)) / 2
    screen.blit(title_words, ((screen.get_width() - title_words.get_width()) / 2, y))
    y += title_words.get_height() + 10
    screen.blit(subtitle_words, ((screen.get_width() - subtitle_words.get_width()) / 2, y))

  def OnCheckResult(self):
    if not self.isover_: return None
    return PageResult(None)


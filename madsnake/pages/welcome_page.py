# MadSnake - A snake game
# Copyright (C) 2020-2021  zhiqiang.bzq
#
# See LICENSE. And if you have interest in this project, join us.
#
# zhiqiang.bzq
# bzq23@foxmail.com

import pygame
from color import Color
from common import *
from pagectr import *

class WelcomePage(object):
  OCCUPATION_LIST = [
    "Thief", "Warrior", "Mage",
  ]
  class Selection(object):
    def __init__(self, id, confirmed):
      self.id = id
      self.confirmed = confirmed

  def __init__(self):
    self.snake_occupation_ = {}

  def OnLoad(self, result):
    self.snake_occupation_.clear()

  def OnKeyboard(self, playerid, action):
    if playerid is None: return
    if playerid not in self.snake_occupation_:
      # selected, confirmed.
      self.snake_occupation_[playerid] = WelcomePage.Selection(0, False)
    else:
      if self.snake_occupation_[playerid].confirmed: return
      if action == Action.UP or action == Action.LEFT:
        self.snake_occupation_[playerid].id = \
              (self.snake_occupation_[playerid].id - 1) % len(WelcomePage.OCCUPATION_LIST)
      elif action == Action.DOWN or action == Action.RIGHT:
        self.snake_occupation_[playerid].id = \
              (self.snake_occupation_[playerid].id + 1) % len(WelcomePage.OCCUPATION_LIST)
      elif action == Action.SKILL:
        self.snake_occupation_[playerid].confirmed = True

  def OnRender(self, screen):
    title_font = pygame.font.SysFont('arial', 32, True)
    welcome_words = title_font.render('Welcome to Mad Snake', True, Color.BROWN, Color.BLACK)
    screen.blit(welcome_words, ((screen.get_width() - welcome_words.get_width()) / 2, screen.get_height() / 3))

    if len(self.snake_occupation_) == 0:
      tips_font = pygame.font.SysFont('arial', 24)
      start_game_words = tips_font.render('Press any keys!', True, Color.RandomColor(), Color.BLACK)
      screen.blit(start_game_words, ((screen.get_width() - start_game_words.get_width()) / 2, screen.get_height() * 2 / 3))
    else:
      # render occupation selections.
      occu_font = pygame.font.SysFont('arial', 24, True)
      if 0 in self.snake_occupation_:
        y = screen.get_height() * 2 / 3
        for occu in WelcomePage.OCCUPATION_LIST:
          occu_words = occu_font.render(occu, True, Color.BLUE, Color.BLACK)
          screen.blit(occu_words, (screen.get_width() / 5, y))
          y += occu_words.get_height() + 5
        occu_words = occu_font.render("P1 >>", True,
            Color.RandomColor() if not self.snake_occupation_[0].confirmed else Color.BLUE, Color.BLACK)
        screen.blit(occu_words, (screen.get_width() / 5 - occu_words.get_width() - 10,
            screen.get_height() * 2 / 3 + self.snake_occupation_[0].id * (occu_words.get_height() + 5)))
      if 1 in self.snake_occupation_:
        y = screen.get_height() * 2 / 3
        for occu in WelcomePage.OCCUPATION_LIST:
          occu_words = occu_font.render(occu, True, Color.PURPLE, Color.BLACK)
          screen.blit(occu_words, (screen.get_width() * 4 / 5 - occu_words.get_width(), y))
          y += occu_words.get_height() + 5
        occu_words = occu_font.render("<< P2", True,
            Color.RandomColor() if not self.snake_occupation_[1].confirmed else Color.PURPLE, Color.BLACK)
        screen.blit(occu_words, (screen.get_width() * 4 / 5 + 10,
            screen.get_height() * 2 / 3 + self.snake_occupation_[1].id * (occu_words.get_height() + 5)))

  def OnCheckResult(self):
    if len(self.snake_occupation_) == 0: return None
    for k, v in self.snake_occupation_.items():
      if not v.confirmed:
        return None
    return PageResult(self.snake_occupation_)


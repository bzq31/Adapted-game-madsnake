# MadSnake - A snake game
# Copyright (C) 2020-2021  zhiqiang.bzq
#
# See LICENSE. And if you have interest in this project, join us.
#
# zhiqiang.bzq
# bzq23@foxmail.com

import pygame
import random

class Color(object):
  RED = pygame.Color(255, 0, 0)
  GREEN = pygame.Color(0, 255, 0)
  BLUE = pygame.Color(0, 0, 255)
  BLACK = pygame.Color(0, 0, 0)
  WHITE = pygame.Color(255, 255, 255)
  GRAY = pygame.Color(150, 150, 150)
  YELLOW = pygame.Color(255, 255, 0)
  PURPLE = pygame.Color(255, 0, 255)
  BROWN = pygame.Color(150, 75, 75)
  DEEP_PINK = pygame.Color(255, 20, 147)
  PINK = pygame.Color(253, 129, 131)
  ORANGE = pygame.Color(251, 193, 93)
  CYAN = pygame.Color(96, 218, 217)

  @classmethod
  def RandomColor(self):
    return pygame.Color(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))


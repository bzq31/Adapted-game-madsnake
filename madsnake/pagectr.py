# MadSnake - A snake game
# Copyright (C) 2020-2021  zhiqiang.bzq
#
# See LICENSE. And if you have interest in this project, join us.
#
# zhiqiang.bzq
# bzq23@foxmail.com

import pygame

class PageResult(object):
  def __init__(self, result):
    self.result = result

class PagesController(object):
  def __init__(self):
    self.pages_ = []
    self.cur_page_ = 0
    self.over_ = False
  
  def AddPage(self, page):
    self.pages_.append(page)

  def OnKeyboard(self, playerid, action):
    if self.cur_page_ >= len(self.pages_): return
    if action == pygame.K_ESCAPE:
      self.over_ = True
    page = self.pages_[self.cur_page_]
    page.OnKeyboard(playerid, action)

  def OnRender(self, screen):
    if self.cur_page_ >= len(self.pages_): return
    page = self.pages_[self.cur_page_]
    page.OnRender(screen)

  def OnCheckOver(self):
    if self.cur_page_ >= len(self.pages_): return True
    page = self.pages_[self.cur_page_]
    result = page.OnCheckResult()
    # page controller will turn to next page when current page return a result.
    if result is not None:
      self.cur_page_ = (self.cur_page_ + 1) % len(self.pages_)
      page = self.pages_[self.cur_page_]
      page.OnLoad(result)
    return self.over_


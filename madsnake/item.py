# MadSnake - A snake game
# Copyright (C) 2020-2021  zhiqiang.bzq
#
# See LICENSE. And if you have interest in this project, join us.
#
# zhiqiang.bzq
# bzq23@foxmail.com

from color import Color
import random

class Item(object):
  # buf item
  SHIELDBUF = 0
  SHOEBUF = 1
  REVIVEBUF = 2
  IRONBUF = 3
  # debuf item
  SLOWDEBUF = 4
  WEAKDEBUF = 5
  NOBUF = 6
  # normal item
  BODY = 7
  SCORE = 8
  # critical item
  WALL = 9
    
  class Info(object):
    def __init__(self, type, name, cname, color, weight, isbuf, isdebuf):
      self.type = type
      self.name = name
      self.cname = cname
      self.color = color
      self.weight = weight
      self.isbuf = isbuf
      self.isdebuf = isdebuf

  ITEM_INFOS = {
    SHIELDBUF : Info(SHIELDBUF, "Shield", "铁布杉", Color.YELLOW, 1, True, False),
    SHOEBUF : Info(SHOEBUF, "Shoe", "神行鞋", Color.ORANGE, 2, True, False),
    REVIVEBUF : Info(REVIVEBUF, "Revive", "复活甲", Color.DEEP_PINK, 1, True, False),
    IRONBUF : Info(IRONBUF, "Iron", "铁头功", Color.CYAN, 1, True, False),
    SLOWDEBUF : Info(SLOWDEBUF, "Slow", "减速", Color.BROWN, 3, False, True),
    WEAKDEBUF : Info(WEAKDEBUF, "Weak", "虚弱", Color.RED, 3, False, True),
    NOBUF : Info(NOBUF, "Palsy", "瘫痪", Color.PINK, 1, False, True),
    BODY : Info(BODY, "Body", "身体", Color.WHITE, 20, False, False),
    SCORE : Info(SCORE, "Score", "分数", Color.GREEN, 10, False, False),
    WALL : Info(WALL, "Wall", "墙", Color.GRAY, 0, False, False),
  }

  def __init__(self, t, loc):
    self.type_ = t
    self.location_ = loc
  def Location(self):
    return self.location_
  def Type(self):
    return self.type_
  def Score(self):
    return 0. if self.type_ != Item.SCORE else 10.
  def Color(self):
    return Item.ColorForType(self.type_)

  @classmethod
  def ColorForType(self, t):
    if t not in Item.ITEM_INFOS: return Color.BLACK
    return Item.ITEM_INFOS[t].color

  @classmethod
  def NameForType(self, t):
    if t not in Item.ITEM_INFOS: return ""
    return Item.ITEM_INFOS[t].name

  @classmethod
  def GenerateRandomItem(self, loc):
    sw = 0
    for info in Item.ITEM_INFOS.values():
      sw += info.weight
    r = random.random()
    for info in Item.ITEM_INFOS.values():
      p = float(info.weight) / sw
      if r < p:
        return Item(info.type, loc)
      else:
        r -= p
    return Item(Item.BODY, loc)

  @classmethod
  def GenerateRandomBufType(self):
    return random.choice([Item.SHIELDBUF, Item.SHOEBUF, Item.REVIVEBUF, Item.IRONBUF])

  @classmethod
  def IsBuf(self, buf):
    if buf not in Item.ITEM_INFOS: return False
    return Item.ITEM_INFOS[buf].isbuf

    

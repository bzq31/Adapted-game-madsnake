# MadSnake - A snake game
# Copyright (C) 2020-2021  zhiqiang.bzq
#
# See LICENSE. And if you have interest in this project, join us.
#
# zhiqiang.bzq
# bzq23@foxmail.com

class Action:
  UP = 0
  DOWN = 1
  LEFT = 2
  RIGHT = 3
  SKILL = 4

class Size:
  def __init__(self, width, height):
    self.width = width
    self.height = height
  def __repr__(self):
    return "(%dx%d)"%(self.width,self.height)

class Point:
  def __init__(self, x, y):
    self.x = x
    self.y = y
  def __repr__(self):
    return "(%d,%d)"%(self.x,self.y)

MAX_SNAKE_COUNT = 2
MIN_MAP_SIZE = Size(20, 20)
MIN_BODY_LEN = 3

class TooManySnakeException(Exception):
  def __str__(self):
        print("Snake count exceed %d"%MAX_SNAKE_COUNT)

class TooSmallMapSizeException(Exception):
  def __str__(self):
        print("Map size too small: %dx%d"%(MIN_MAP_SIZE.width, MIN_MAP_SIZE.height))


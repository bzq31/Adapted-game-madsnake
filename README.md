# MadSnake

#### 介绍
MadSnake 基于 pygame，实现了功能丰富的贪吃蛇开发框架。

#### 安装教程

```
git clone https://gitee.com/bzq31/Adapted-game-madsnake.git
cd Adapted-game-madsnake
python3 ./madsnake/main.py 
```

#### 玩法说明

1.  游戏目前只支持**单人**或**双人**；
2.  游戏开始，玩家可从**三个职业**中选择一个，每个职业有不同的技能：
	* **盗贼**：可偷取范围内（蛇头与蛇头距离5）所有其它蛇的一个增益BUF;
	* **战士**：双倍化身上增益BUF的剩余时间；
	* **法师**：随机获得一个增益BUF；
	* **所有职业技能CD为60秒**；
3.  地图**每5秒**会刷新一种补给物品，物品分为**增益BUF**，**减益DEBUF**，以及**普通物件**：
	* 增益BUF包括：
		* 铁布杉/Shield/黄色：免疫**七寸**攻击；
		* 神行鞋/Shoe/	橙色：移速增加；
		* 复活甲/Revive/深粉色：被**七寸**攻击时，断尾逃生；
		* 铁头功/Iron/青色：与没有IronBUF的对手**头对头相碰**时存活（如果双方都有该BUF，一起挂掉）
	* 减益DEBUF包括：
		* 减速/Slow/棕色：移速大幅度降低；
		* 虚弱/Weak/红色：身体任意部份都被视为是**七寸**；
		* 瘫痪/Palsy/粉色：不能再获取任意补给（但是会清掉地图补给，只是BUF不生效，之前获取的不受影响）；
	* 普通物件包括：
		* 身体/Body/白色：捡到后身体加长1格；
		* 分数/Score/绿色：增加10分；
	* **每种BUF的持续时间为10秒**；
	* **每种补给物品在地图上持续时间为30秒，若30秒内无人捡取则会消失**；
4.  **七寸**机制：
	* 在无任何buf/debuf下，当身体长度>=7时，身体的第7格会被视为蛇的**七寸**;
	* 当buf/debuf种类总数超过5时，每超过1种，七寸位置往后移1格（如当前有7种状态，七寸位置往后移至 7 + (7 - 5) = 9 的位置);
	* 当蛇A头碰到另外一条蛇B的七寸，视为蛇A对蛇B发动七寸攻击；
	* 在正常情况下，**被七寸攻击的蛇当场死亡**（除非有Shield/Revive保护）；
4.  如何判断游戏结束以及输赢：
	* 蛇的死亡有且只有以下四种情况：
		* 蛇头撞墙；
		* 蛇头撞到其它蛇头，且无IronBUF；
		* 蛇头撞到其它蛇非蛇身处，且被撞蛇无WeakDEBUF；
		* 被七寸攻击，且无Shield/ReviveBUF;
	* 游戏结束有且只有以下三种情况：
		* 所有蛇均死亡 —— 平局
		* 场上只有一只蛇存活 —— 该蛇胜出
		* 游戏倒计时结束(180秒) —— 分数最高的蛇胜出（如果有多条蛇同时高分，算平局）

#### 操作键
1.	每个玩家只响应5个键（上/下/左/右 技能） 分别是：
	* Player1： w键/s键/a键/d键  f键 
	* Player2： 上键/下键/左键/右键  /键
2. 在**欢迎界面按ESC退出**；
3. 在**结束界面按空格重新开始**；

#### 界面展示
<img src="welcome.jpg" width = "150" height = "200" alt="welcome"/>
<img src="occuselect.jpg" width = "150" height = "200" alt="occuselect"/>
<img src="gameplaying.jpg" width = "150" height = "200" alt="gameplaying"/>
<img src="gameplayingtip.jpg" width = "150" height = "200" alt="gameplayingtip"/>
<img src="gameover.jpg" width = "150" height = "200" alt="gameover"/>

#### TODO
1. 多人联机 PK，支持多人 (2-4人) 加入；
2. 多人AI蛇对局支持；

想一起搞事情吗？联系 bzq23@foxmail.com

